/*
 * $Id$
 */
#include <stdlib.h>

#include <Xm/XmAll.h>

int main(int argc, char *argv[]) {
	XtAppContext appContext;
	const char *applicationClass = "XmFontTest";
	String fallbackResources[] = {
//			"*.label.fontList: -monotype-arial-medium-r-normal--*-120-*-*-p-*-iso8859-5",
//			"*.label.fontList: -monotype-arial-medium-r-normal--*-120-*-*-p-*-ibm-cp866",
//			"*.label.fontList: -monotype-arial-medium-r-normal--*-120-*-*-p-*-koi8-r",
//			"*.label.fontList: -monotype-arial-medium-r-normal--*-120-*-*-p-*-microsoft-cp1251",
			"*.label.fontList: -monotype-arial-medium-r-normal--*-120-*-*-p-*-iso10646-1",
			NULL,
	};
	const Widget topLevel = XtVaAppInitialize(&appContext, applicationClass, NULL, 0, &argc, argv, fallbackResources,
			XmNtitle, "Font Test, " XmVERSION_STRING,
			XmNiconName, "Font Test, " XmVERSION_STRING " (Icon)",
			NULL);

	const Widget mainWindow = XmVaCreateManagedMainWindow(topLevel, "mainWindow", NULL);

	const Widget label = XmVaCreateManagedLabel(mainWindow, "label", NULL);

#if 0
	const XmString labelString = XmStringCreateLocalized("XmString created using XmStringCreateLocalized(): \u0410\u0411\u0412\u0413\u0414\u0415\u0401\u0416\u0417\u0418\u0419\u041a\u041b\u041c\u041d\u041e\u041f\u0420\u0421\u0422\u0423\u0424\u0425\u0426\u0427\u0428\u0429\u042a\u042b\u042c\u042d\u042e\u042f\u0430\u0431\u0432\u0433\u0434\u0435\u0451\u0436\u0437\u0438\u0439\u043a\u043b\u043c\u043d\u043e\u043f\u0440\u0441\u0442\u0443\u0444\u0445\u0446\u0447\u0448\u0449\u044a\u044b\u044c\u044d\u044e\u044f");
#else
//	const XmStringCharSet stringCharset = XmSTRING_DEFAULT_CHARSET;
//	const XmStringCharSet stringCharset = XmSTRING_ISO8859_1;
	const XmStringCharSet stringCharset = "UTF-8";
	const XmString labelString = XmStringCreate("XmString created using XmStringCreate(): \u0410\u0411\u0412\u0413\u0414\u0415\u0401\u0416\u0417\u0418\u0419\u041a\u041b\u041c\u041d\u041e\u041f\u0420\u0421\u0422\u0423\u0424\u0425\u0426\u0427\u0428\u0429\u042a\u042b\u042c\u042d\u042e\u042f\u0430\u0431\u0432\u0433\u0434\u0435\u0451\u0436\u0437\u0438\u0439\u043a\u043b\u043c\u043d\u043e\u043f\u0440\u0441\u0442\u0443\u0444\u0445\u0446\u0447\u0448\u0449\u044a\u044b\u044c\u044d\u044e\u044f", stringCharset);
#endif // 0
	XtVaSetValues(label, XmNlabelString, labelString, NULL);
//	XtVaSetValues(label, XmNfontList, "-*-times-medium-i-normal--*-*-*-*-*-*-iso8859-1", NULL);
	XmStringFree(labelString);

	XtRealizeWidget(topLevel);
	XtAppMainLoop(appContext);

	return 0;
}
